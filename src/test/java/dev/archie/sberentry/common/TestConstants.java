package dev.archie.sberentry.common;

import dev.archie.sberentry.client.Client;
import dev.archie.sberentry.client.dto.CreatingClientDto;
import dev.archie.sberentry.client.dto.UpdatingClientDto;

public class TestConstants {

    public static final CreatingClientDto CREATING_CLIENT_DTO = CreatingClientDto.builder()
            .firstName("Name")
            .lastName("Last Name")
            .patronymic("Patronymic")
            .description("Updated")
            .build();

    public static final UpdatingClientDto UPDATING_CLIENT_DTO = UpdatingClientDto.builder()
            .firstName("Name")
            .lastName("Last Name")
            .patronymic("Patronymic")
            .description("Description")
            .build();

    public static final Client MOCK_CLIENT = Client.builder()
            .firstName("Name")
            .lastName("Last Name")
            .patronymic("Patronymic")
            .description("Description")
            .build();

}
