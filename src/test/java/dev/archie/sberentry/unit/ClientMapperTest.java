package dev.archie.sberentry.unit;

import dev.archie.sberentry.client.Client;
import dev.archie.sberentry.client.ClientMapper;
import dev.archie.sberentry.common.TestConstants;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.mapstruct.factory.Mappers;

public class ClientMapperTest {

    private static ClientMapper clientMapper;

    @BeforeAll
    static void beforeAll() {
        clientMapper = Mappers.getMapper(ClientMapper.class);
    }

    @Test
    void toClient_shouldMapCreatingClientDto() {
        Client client = clientMapper.toClient(TestConstants.CREATING_CLIENT_DTO);
        Assertions.assertEquals(TestConstants.CREATING_CLIENT_DTO.getDescription(), client.getDescription());
        Assertions.assertEquals(TestConstants.CREATING_CLIENT_DTO.getLastName(), client.getLastName());
        Assertions.assertEquals(TestConstants.CREATING_CLIENT_DTO.getFirstName(), client.getFirstName());
        Assertions.assertEquals(TestConstants.CREATING_CLIENT_DTO.getPatronymic(), client.getPatronymic());
    }

    @Test
    void toClient_shouldMapUpdatingClientDto() {
        Client client = clientMapper.toClient(TestConstants.UPDATING_CLIENT_DTO);
        Assertions.assertEquals(TestConstants.UPDATING_CLIENT_DTO.getDescription(), client.getDescription());
        Assertions.assertEquals(TestConstants.UPDATING_CLIENT_DTO.getLastName(), client.getLastName());
        Assertions.assertEquals(TestConstants.UPDATING_CLIENT_DTO.getFirstName(), client.getFirstName());
        Assertions.assertEquals(TestConstants.UPDATING_CLIENT_DTO.getPatronymic(), client.getPatronymic());
    }
}
