package dev.archie.sberentry.integration;


import dev.archie.sberentry.client.ClientRepository;
import dev.archie.sberentry.common.TestConstants;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public abstract class AbstractIntegrationTest {

    @Autowired
    private ClientRepository clientRepository;

    @BeforeEach
    void beforeAll() {
        Long id = clientRepository.save(TestConstants.MOCK_CLIENT).getId();
        TestConstants.MOCK_CLIENT.setId(id);
    }

    @AfterEach
    void afterAll() {
        if (clientRepository.existsById(TestConstants.MOCK_CLIENT.getId())) {
            clientRepository.deleteById(TestConstants.MOCK_CLIENT.getId());
        }
    }
}
