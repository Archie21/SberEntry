package dev.archie.sberentry.integration;

import com.fasterxml.jackson.databind.ObjectMapper;
import dev.archie.sberentry.client.ClientRepository;
import dev.archie.sberentry.common.TestConstants;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@SpringBootTest
@AutoConfigureMockMvc
public class ClientControllerTest extends AbstractIntegrationTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private ClientRepository clientRepository;

    @Test
    void postClients_shouldCreateNewClient() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/clients")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(TestConstants.CREATING_CLIENT_DTO)))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.firstName").value(TestConstants.CREATING_CLIENT_DTO.getFirstName()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.lastName").value(TestConstants.CREATING_CLIENT_DTO.getLastName()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.patronymic").value(TestConstants.CREATING_CLIENT_DTO.getPatronymic()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.description").value(TestConstants.CREATING_CLIENT_DTO.getDescription()));
    }

    @Test
    void getClients_shouldReturnExistingClient() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/clients/" + TestConstants.MOCK_CLIENT.getId())
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.firstName").value(TestConstants.MOCK_CLIENT.getFirstName()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.lastName").value(TestConstants.MOCK_CLIENT.getLastName()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.patronymic").value(TestConstants.MOCK_CLIENT.getPatronymic()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.description").value(TestConstants.MOCK_CLIENT.getDescription()));
    }

    @Test
    void getClients_shouldReturnClientsWithPagination_whenGivenParams() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/clients")
                        .contentType(MediaType.APPLICATION_JSON)
                        .param("firstName", TestConstants.MOCK_CLIENT.getFirstName())
                        .param("pageNumber", "0")
                        .param("pageSize", "10"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.content[0].firstName").value(TestConstants.MOCK_CLIENT.getFirstName()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.content[0].lastName").value(TestConstants.MOCK_CLIENT.getLastName()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.content[0].patronymic").value(TestConstants.MOCK_CLIENT.getPatronymic()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.content[0].description").value(TestConstants.MOCK_CLIENT.getDescription()));
    }

    @Test
    void putClients_shouldUpdateClient() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.put("/clients/" + TestConstants.MOCK_CLIENT.getId())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(TestConstants.UPDATING_CLIENT_DTO)))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.firstName").value(TestConstants.UPDATING_CLIENT_DTO.getFirstName()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.lastName").value(TestConstants.UPDATING_CLIENT_DTO.getLastName()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.patronymic").value(TestConstants.UPDATING_CLIENT_DTO.getPatronymic()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.description").value(TestConstants.UPDATING_CLIENT_DTO.getDescription()));
    }

    @Test
    void deleteClients_shouldDeleteClient() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.delete("/clients/" + TestConstants.MOCK_CLIENT.getId()))
                .andExpect(MockMvcResultMatchers.status().isOk());
        Assertions.assertFalse(clientRepository.findById(TestConstants.MOCK_CLIENT.getId()).isPresent());
    }

}
