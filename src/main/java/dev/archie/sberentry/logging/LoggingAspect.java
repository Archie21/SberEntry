package dev.archie.sberentry.logging;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * Aspect for logging method calls.
 */
@Aspect
@Component
public class LoggingAspect {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    /**
     * Logs the method call and its execution time, if the method is annotated with @Loggable.
     *
     * @param joinPoint the join point representing the method call
     * @return the result of the method call
     * @throws Throwable if the method throws an exception
     */
    @Around("@annotation(dev.archie.sberentry.logging.Loggable)")
    public Object logMethodCall(ProceedingJoinPoint joinPoint) throws Throwable {
        MethodSignature signature = (MethodSignature) joinPoint.getSignature();
        String methodName = signature.getName();
        String className = joinPoint.getTarget().getClass().getSimpleName();

        logger.info("Calling method {} in class {}", methodName, className);
        long startExecutionTime = System.currentTimeMillis();
        Object result = joinPoint.proceed();
        long endExecutionTime = System.currentTimeMillis();
        long executionTime = endExecutionTime - startExecutionTime;
        logger.info("Method {} in class {} returned {}, execution time in ms: {}", methodName, className, result, executionTime);
        return result;
    }

}
