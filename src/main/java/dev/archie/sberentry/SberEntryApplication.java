package dev.archie.sberentry;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SberEntryApplication {

    public static void main(String[] args) {
        SpringApplication.run(SberEntryApplication.class, args);
    }

}
