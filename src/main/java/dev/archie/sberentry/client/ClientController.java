package dev.archie.sberentry.client;

import dev.archie.sberentry.client.dto.CreatingClientDto;
import dev.archie.sberentry.client.dto.UpdatingClientDto;
import dev.archie.sberentry.logging.Loggable;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * Controller for handling requests related to clients.
 */
@RestController
@RequestMapping("/clients")
@RequiredArgsConstructor
public class ClientController {

    private final ClientService clientService;

    /**
     * Creates a new client with the provided data.
     *
     * @param clientDto the data of the client to be created
     * @return the created client
     */
    @Loggable
    @PostMapping
    public Client create(@RequestBody CreatingClientDto clientDto) {
        return clientService.create(clientDto);
    }

    /**
     * Retrieves a client by its id.
     *
     * @param id the id of the client to be retrieved
     * @return the retrieved client
     */
    @Loggable
    @GetMapping("/{id}")
    public Client getById(@PathVariable Long id) {
        return clientService.getById(id);
    }

    /**
     * Searches for clients based on the provided parameters.
     *
     * @param params the search parameters
     * @return a page of clients that match the search criteria
     */
    @Loggable
    @GetMapping
    public Page<Client> searchByParams(@RequestParam Map<String, String> params) {
        return clientService.search(params);
    }

    /**
     * Updates the data of an existing client.
     *
     * @param id        the id of the client to be updated
     * @param clientDto the updated data of the client
     * @return the updated client
     */
    @Loggable
    @PutMapping("/{id}")
    public Client update(@PathVariable Long id, @RequestBody UpdatingClientDto clientDto) {
        return clientService.update(clientDto, id);
    }

    @Loggable
    @DeleteMapping("/{id}")
    public void delete(@PathVariable Long id) {
        clientService.deleteById(id);
    }

}
