package dev.archie.sberentry.client;

import dev.archie.sberentry.client.dto.CreatingClientDto;
import dev.archie.sberentry.client.dto.UpdatingClientDto;
import dev.archie.sberentry.client.exception.ClientNotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.Map;
import java.util.function.Function;

/**
 * Service for handling operations related to clients.
 */
@Service
@RequiredArgsConstructor
public class ClientService {

    private final ClientRepository clientRepository;
    private final ClientMapper clientMapper;


    /**
     * Creates a new client with the provided data and saves it to the repository.
     *
     * @param creatingClientDto the data of the client to be created
     * @return the created client
     */
    public Client create(CreatingClientDto creatingClientDto) {
        Client client = clientMapper.toClient(creatingClientDto);
        return clientRepository.save(client);
    }

    /**
     * Retrieves a client by its id from the repository.
     *
     * @param id the id of the client to be retrieved
     * @return the retrieved client
     * @throws ClientNotFoundException if the client with the specified id is not found
     */
    public Client getById(Long id) {
        return clientRepository.findById(id)
                .orElseThrow(() -> new ClientNotFoundException(id));
    }

    /**
     * Searches for clients based on the provided parameters.
     *
     * @param params the search parameters, should also contains optional pageNumber and pageSize keys.
     * @return a page of clients that match thesearch criteria
     */
    public Page<Client> search(Map<String, String> params) {
        int pageNumber = Integer.parseInt(params.getOrDefault("pageNumber", "0"));
        int pageSize = Integer.parseInt(params.getOrDefault("pageSize", "10"));
        params.remove("pageNumber");
        params.remove("pageSize");

        Specification<Client> specification = searchByParams(params);
        Pageable pageable = PageRequest.of(pageNumber, pageSize);
        return clientRepository.findAll(specification, pageable);
    }

    /**
     * Updates the data of an existing client and saves it to the repository.
     *
     * @param updatingClientDto the updated data of the client
     * @param id                the id of the client to be updated
     * @return the updated client
     * @throws ClientNotFoundException if the client with the specified id is not found
     */
    public Client update(UpdatingClientDto updatingClientDto, Long id) {
        Client client = getById(id);
        Client clientToUpdate = clientMapper.toClient(updatingClientDto);
        clientToUpdate.setId(client.getId());
        return clientRepository.save(clientToUpdate);
    }

    /**
     * Deletes a client by its id from the repository.
     *
     * @param id the id of the client to be deleted
     * @throws ClientNotFoundException if the client with the specified id is not found
     */
    public void deleteById(Long id) {
        Client client = getById(id);
        clientRepository.delete(client);
    }

    /**
     * Generates a JPA specification based on the provided search parameters.
     *
     * @param params the search parameters
     * @return a JPA specification for the search
     */
    private Specification<Client> searchByParams(Map<String, String> params) {
        return (client, criteriaQuery, criteriaBuilder) -> criteriaBuilder.and(params.entrySet()
                .stream()
                .map(fieldMatches(client, criteriaBuilder))
                .toArray(Predicate[]::new));
    }

    /**
     * Generates a predicate for a search parameter.
     *
     * @param client          the root of the query
     * @param criteriaBuilder the criteria builder
     * @return a predicate for a search parameter
     */
    private static Function<Map.Entry<String, String>, Predicate> fieldMatches(Root<Client> client, CriteriaBuilder criteriaBuilder) {
        return entry -> criteriaBuilder.equal(client.get(entry.getKey()), entry.getValue());
    }
}
