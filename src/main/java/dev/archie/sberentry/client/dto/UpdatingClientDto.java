package dev.archie.sberentry.client.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class UpdatingClientDto {
    private String firstName;
    private String lastName;
    private String patronymic;
    private String description;
}