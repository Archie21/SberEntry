package dev.archie.sberentry.client;

import dev.archie.sberentry.client.dto.CreatingClientDto;
import dev.archie.sberentry.client.dto.UpdatingClientDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingConstants;

@Mapper(componentModel = MappingConstants.ComponentModel.SPRING)
public interface ClientMapper {
    @Mapping(target = "id", ignore = true)
    Client toClient(UpdatingClientDto updatingClientDto);

    @Mapping(target = "id", ignore = true)
    Client toClient(CreatingClientDto updatingClientDto);

}
