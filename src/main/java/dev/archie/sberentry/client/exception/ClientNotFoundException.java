package dev.archie.sberentry.client.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

public class ClientNotFoundException extends ResponseStatusException {
    public ClientNotFoundException(Long id) {
        super(HttpStatus.NOT_FOUND, "No such client with id: " + id);
    }
}
